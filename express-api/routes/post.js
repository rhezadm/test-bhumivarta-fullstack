const express = require('express');
const router = express.Router();

//import database
const connection = require('../config/database');

/**
 * INDEX POSTS
 */
router.get('/', function (req, res) {
    //query
    connection.query('SELECT longitude,latitude,brand,time,COUNT(*) as user_brand_count FROM `tbl_lokasi` WHERE time BETWEEN "2021-10-20 07:00:00" AND "2021-10-20 08:00:00" group by longitude,latitude,brand', function (err, rows) {
        if (err) {
            return res.status(500).json({
                status: false,
                message: 'Internal Server Error',
            })
        } else {
            return res.status(200).json({
                status: true,
                message: 'List Data Posts',
                data: rows
            })
        }
    });
});

module.exports = router;